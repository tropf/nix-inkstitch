# Ink/Stitch for nix
This repository is a [nix flake](https://nixos.org/manual/nix/stable/command-ref/new-cli/nix3-flake.html) for the [Ink/Stitch](https://inkstitch.org/) [Inkscape](https://inkscape.org/) extension, version 3.1.0.
It can be used to create designs for machine embroidery.

***

You probably want to use the [nixpkgs version of inkstitch](https://search.nixos.org/packages?channel=unstable&show=inkscape-extensions.inkstitch&type=packages) (available in unstable/from 25.05).

```bash
nix-shell -p 'inkscape-with-extensions.override {inkscapeExtensions = [ inkscape-extensions.inkstitch ]; }' --command inkscape
```

***

For some time, this repository has been the sole place to get a nix package of inkstitch.
Now, please use the nixpkgs version.
This repository lives on as a place to tinker with inkstitch packaging in the nix ecosystem.

## Content & Running this Flake
The flake provides the binary `inkscape-inkstitch`, where you can find Ink/Stitch in the *Extensions* menu.
Installing this flake will **not** add Ink/Stitch to you **default** inkscape installation.

Run with:

```bash
nix run git+https://codeberg.org/tropf/nix-inkstitch
```

nix will offer to add `https://hydra.hq.c3d2.de` as a binary cache.

## Notes on Packaging
As user you should use the `inkscape-inkstitch` package.
It is configured as default.
The flake also contains further packages for internal use/debugging.
The following packages are available (see `nix flake show`):

- `inkscape-inkstitch`: inkscape bundled with Ink/Stitch
- `pyembroidery`: python embroidery library, pulled from PyPI (does **not** use the version bundled with Ink/Stitch)
- `inkstitch-python-env`: a python environment where all inkstitch dependencies (including `pyembroidery`) is available
- `inkstitch`: Ink/Stitch itself (only the inkscape extension)

The packaging follows [the official guide for manual setup](https://inkstitch.org/developers/inkstitch/manual-setup/).
It consists of the following main parts:

1. Prepare Python environment (including the `pyembroidery` library)
2. Generate the `.inx` files
3. Patch invocation to inject python environment as shebang.
4. Combine everything through symlinking, add a wrapper to inject required binaries/python dependencies.

For this to properly work on nix, several patches are required.
Please refer to the [`patches/` subdirectory](patches/) of this flake.

## Troubleshooting
- Is there an Inkstitch in `~/.config/inkscape/extensions/`?  
  If yes, remove -- both inkstitches conflict.
- Inkscape is unresponsive when Ink/Stitch is open.  
  This is known and wont be fixed.
- All menu items are greyed out:  
  Please report. This is an installation issue, and related to Inkscape not finding the correct command to execute.
  This command is given in `.inx` files, and does not point to the correct location.

## License
The code in this repository is licensed under [GPLv3 or later](./COPYING).
Please note that the libraries referenced by this flake may use a different license.

## Online
Find this repository online at: <https://codeberg.org/tropf/nix-inkstitch>
